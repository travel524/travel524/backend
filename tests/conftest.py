from typing import TYPE_CHECKING

import pytest

from app.core.config import get_app_settings
from main import get_application

if TYPE_CHECKING:  # pragma: no cover
    from fastapi import FastAPI

    from app.core.settings.app import AppSettings

# Подключается фикстуры и фабрики
fixtures = [
    "fixtures.db",  # Подготовка тестовой базы
    "fixtures.overrides",  # override зависимостей
    "fixtures.clients",  # HTTP клиенты для выполнения запросов к приложению
]
pytest_plugins = [
    *(f"tests.{fixture_plugin}" for fixture_plugin in fixtures),
]


@pytest.fixture()
def app() -> "FastAPI":
    # Не используем scope=session так как нужно иногда оверрайдить зависимости, и так они будут автоматически очищаться
    return get_application()


@pytest.fixture(scope="session")
def settings() -> "AppSettings":
    return get_app_settings()
