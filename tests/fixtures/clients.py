from typing import TYPE_CHECKING

import pytest
from fastapi.testclient import TestClient

if TYPE_CHECKING:  # pragma: no cover
    from fastapi import FastAPI


@pytest.fixture()
def api_client(app: "FastAPI") -> TestClient:
    """Клиент для отправки запросов."""
    return TestClient(app, base_url="http://test")
