from typing import TYPE_CHECKING

import pytest
from alembic.command import upgrade as alembic_upgrade
from alembic.config import Config as AlembicConfig
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy_utils import create_database, database_exists, drop_database

if TYPE_CHECKING:  # pragma: no cover
    from collections.abc import Generator

    from sqlalchemy import Connection, Engine
    from sqlalchemy.orm import Session

    from app.core.settings.app import AppSettings


@pytest.fixture(scope="session")
def engine(settings: "AppSettings") -> "Engine":
    return create_engine(str(settings.database_uri))


@pytest.fixture()
def connection(engine: "Engine") -> "Generator[Connection, None, None]":
    with engine.begin() as connection:
        yield connection
        connection.rollback()


@pytest.fixture()
def session_factory(connection: "Connection") -> scoped_session["Session"]:
    return scoped_session(
        sessionmaker(
            expire_on_commit=False,
            autoflush=False,
            bind=connection,
            join_transaction_mode="create_savepoint",
        ),
    )


@pytest.fixture()
def session(session_factory: scoped_session["Session"]) -> "Generator[Session, None, None]":
    session: "Session" = session_factory()
    yield session
    session.close()


@pytest.fixture(scope="session", autouse=True)
def _setup_database(engine: "Engine", settings: "AppSettings"):
    if not database_exists(str(settings.database_uri)):
        create_database(str(settings.database_uri))

    alembic_config = AlembicConfig("alembic.ini")
    alembic_config.set_main_option("sqlalchemy.url", str(settings.database_uri))

    with engine.connect() as connection:
        alembic_config.attributes["connection"] = connection
        alembic_upgrade(alembic_config, "head")

    yield

    drop_database(str(settings.database_uri))
