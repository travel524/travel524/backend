from typing import TYPE_CHECKING

import pytest

if TYPE_CHECKING:  # pragma: no cover
    from fastapi import FastAPI
    from sqlalchemy.orm import Session


@pytest.fixture(autouse=True)
def _override_get_session(app: "FastAPI", session: "Session"):
    from app.database import get_session

    def _get_session():
        return session

    app.dependency_overrides[get_session] = _get_session

    yield

    app.dependency_overrides = {}
