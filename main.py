import uvicorn
from fastapi import FastAPI
from fastapi_pagination import add_pagination

from app.api.v1.api import api_router
from app.core.config import settings


def get_application() -> FastAPI:
    application = FastAPI(title=settings.project_name)
    application.include_router(api_router)
    add_pagination(application)
    return application


app = get_application()

if __name__ == "__main__":  # pragma: no cover
    uvicorn.run(app, host=settings.internal_host, port=settings.internal_port)
