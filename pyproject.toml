[tool.poetry]
name = ""
version = "0.0.0"
description = ""
authors = ["Danila Anisimov <da.anisimov@bi.zone>"]

[tool.poetry.dependencies]
python = "^3.11"
fastapi = "^0.103.2"
alembic = "^1.12.0"
sqlalchemy = "^2.0.21"
uvicorn = "^0.23.2"
yarl = "^1.9.2"
loguru = "^0.7.2"
psycopg = { extras = ["binary"], version = "^3.1.12" }
pydantic = "^2.4.2"
fastapi-pagination = "^0.12.10"
pydantic-core = "^2.10.1"
more-itertools = "^10.1.0"
pydantic-settings = "^2.1.0"

[tool.poetry.group.dev.dependencies]
ruff = "^0.0.292"
black = "^23.9.1"
mypy = "^1.5.1"
pre-commit = "^3.5.0"
deptry = "^0.12.0"
types-requests = "^2.31.0.8"
pytest-lazy-fixtures = "^1.0.1"

[tool.poetry.group.test.dependencies]
sqlalchemy-utils = "^0.41.1"
pytest = "^7.4.0"
factory-boy = "^3.3.0"
pytest-env = "^0.8.2"
pytest-mock = "^3.11.1"
pytest-cov = "^4.1.0"
requests-mock = "^1.11.0"
pytest-httpx = "^0.26.0"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.black]
line-length = 120
target-version = ["py311"]
exclude = '''
/(
    \.git
  | \.venv
  | .*\/migrations

)/
'''

[tool.mypy]
plugins = ["pydantic.mypy", "sqlalchemy.ext.mypy.plugin"]

follow_imports = "silent"
warn_redundant_casts = true
warn_unused_ignores = true
disallow_any_generics = true
check_untyped_defs = true
no_implicit_reexport = true
ignore_missing_imports = true

[tool.pydantic-mypy]
init_forbid_extra = true
init_typed = true
warn_required_dynamic_aliases = true
warn_untyped_fields = true

[tool.ruff]
select = [
    "F",   # pyflakes
    "E",   # pycodestyle errors
    "W",   # pycodestyle warnings
    "C90", # mccabe
    "I",   # isort
    "N",   # pep8-naming
    # "D", # pydocstyle
    "UP",  # pyupgrade
    "YTT", # flake8-2020
    #    "ANN", # flake8-annotations
    "S",   # bandit
    "BLE", # flake8-blind-except
    "FBT", # flake8-boolean-trap
    "B",   # flake8-bugbear
    #    "A", # flake8-builtins
    "COM", # flake8-commas
    "C4",  # flake8-comprehensions
    "DTZ", # flake8-datetimez
    "T10", # flake8-debugger
    "DJ",  # flake8-django
    "EM",  # flake8-errmsg
    "EXE", # flake8-executable
    "ISC", # flake8-implicit-str-concat
    "ICN", # flake8-import-conventions
    "G",   # flake8-logging-format
    "INP", # flake8-no-pep420
    "PIE", # flake8-pie
    "T20", # flake8-print
    "PYI", # flake8-pyi
    "PT",  # flake8-pytest-style
    "Q",   # flake8-quotes
    "RSE", # flake8-raise
    "RET", # flake8-return
    # "SLF",  # flake8-sel
    "SLOT", # flake8-slots
    "SIM",  # flake8-simplify
    "TID",  # flake8-tidy-imports
    "TCH",  # flake8-type-checking
    "INT",  # flake8-gettext
    #    "ARG", # flake8-unused-arguments
    "PTH", # flake8-use-pathlib
    # "TD",  # flake8-todos
    "FIX", # flake8-fixme
    #    "ERA", # eradicate
    "PL",   # Pylint
    "TRY",  # tryceratops
    "FLY",  # flynt
    "PERF", # Perflint
]
ignore = [
    "B008",   # do not perform function calls in argument defaults
    "C901",   # too complex
    "DJ001",  # too late for it :(
    "RET504", # didnt work properly
    "FIX002", # TODO using
]

# Exclude a variety of commonly ignored directories.
exclude = [".git", ".venv", "*/migrations/*"]

# Same as Black.
line-length = 120

# Assume Python 3.11.
target-version = "py311"

[tool.ruff.per-file-ignores]
"*tests/*" = [
    "PLR2004",
    "S",       # проверку безопасности в тестах
    "FBT",     # позиционные логичские аргументы
    "PLR0913", # количество аргументов
    "ARG",     # неиспользуевые аргументы
    "DTZ",     # таймзоны
]
"settings.py" = [
    "PTH118",  # в settings можно НЕ использовать Path
    "PLW1508", # ignore type for default env value
]


[tool.ruff.mccabe]
# Unlike Flake8, default to a complexity level of 10.
max-complexity = 10

[tool.ruff.pep8-naming]
# Allow Pydantic's `@validator` decorator to trigger class method treatment.
classmethod-decorators = [
    "classmethod",
    "pydantic.validator",
    "pydantic.root_validator",
]

[tool.pytest.ini_options]
console_output_style = "count"
testpaths = ["tests/"]
python_files = ["tests.py", "test_*.py", "*_tests.py"]
env = ["APP_ENV=test"]

[tool.coverage.run]
source = ["app"]
omit = ["*/migrations/*"]

[tool.coverage.report]
show_missing = true
skip_covered = true
skip_empty = true
omit = ["*/tests/*", "*/migrations/*"]
sort = "-Miss"
fail_under = 80

[tool.deptry]
exclude = ["venv", ".venv", ".direnv", "tests", ".git", "Docker", "dockertests"]

[tool.deptry.per_rule_ignores]
DEP002 = ["uvicorn", "psycopg"]
