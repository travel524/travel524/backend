import datetime
from enum import StrEnum

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from app.database import Base


class Gender(StrEnum):
    MALE = "male"
    FEMALE = "female"
    UNDEFINED = "undefined"


class Tag(StrEnum):
    BEACH = "beach"
    EXCURSION = "excursion"
    ADVENTURE = "adventure"
    FOOD = "food"
    NATURE = "nature"
    CULTURE = "culture"
    SPORT = "sport"
    MEDICINE = "medicine"
    CRUISE = "cruise"
    SPA = "spa"
    FITNESS = "fitness"


class Feed(StrEnum):
    BREAKFAST = "breakfast"
    FULL = "full"
    NONE = "none"


class Tour(Base):
    __tablename__ = "tour"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=False)
    name: Mapped[str]
    description: Mapped[str]
    places: Mapped[int]
    tags: Mapped[Tag]
    country: Mapped[str]
    city: Mapped[str]
    price: Mapped[str]
    start_date: Mapped[datetime.date]
    end_date: Mapped[datetime.date]
    rating: Mapped[float]
    hotel: Mapped[int] = mapped_column(ForeignKey("hotel.id"), nullable=True)


class Hotel(Base):
    __tablename__ = "hotel"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=False)
    feed: Mapped[Feed]
    conditioner: Mapped[int] = mapped_column(default=0)
    washing_machine: Mapped[int] = mapped_column(default=0)
    safe: Mapped[int] = mapped_column(default=0)
    mini_bar: Mapped[int] = mapped_column(default=0)


class CreditCard(Base):
    __tablename__ = "credit_card"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    number: Mapped[str]
    date: Mapped[datetime.date]


class User(Base):
    __tablename__ = "user"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    first_name: Mapped[str]
    last_name: Mapped[str]
    surname: Mapped[str] = mapped_column(nullable=True, server_default=None)
    birthdate: Mapped[datetime.date]
    email: Mapped[str]
    gender: Mapped[Gender]
    country: Mapped[str]
    credit_card: Mapped[int] = mapped_column(ForeignKey("credit_card.id"), nullable=True)


class SupportMessage(Base):
    __tablename__ = "support_message"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    user: Mapped[int] = mapped_column(ForeignKey("user.id"))
    message: Mapped[str]


class UserCart(Base):
    __tablename__ = "user_cart"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    user: Mapped[int] = mapped_column(ForeignKey("user.id"))
    tour: Mapped[int] = mapped_column(ForeignKey("tour.id"))


class BoughtTours(Base):
    __tablename__ = "bought_tours"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    user: Mapped[int] = mapped_column(ForeignKey("user.id"))
    tour: Mapped[int] = mapped_column(ForeignKey("tour.id"))
