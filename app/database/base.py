from collections.abc import Generator
from contextlib import contextmanager
from typing import TYPE_CHECKING

from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase, sessionmaker

from app.core.config import settings

if TYPE_CHECKING:  # pragma: no cover
    from sqlalchemy.orm import Session


engine = create_engine(str(settings.database_uri))
SessionLocal = sessionmaker(bind=engine, autoflush=False, expire_on_commit=False)


class Base(DeclarativeBase):
    pass


def get_session():
    db = SessionLocal()
    try:
        yield db
    except:  # noqa: E722
        db.close()


session_factory = contextmanager(get_session)
