from datetime import date

from pydantic import BaseModel

from app.database.tables import Feed, Gender, Tag


class TourCreate(BaseModel):
    name: str
    description: str
    places: int
    tags: list[Tag]
    country: str
    city: str
    price: str
    start_date: date
    end_date: date
    rating: float
    hotel: int | None = None


class HotelCreate(BaseModel):
    feed: Feed
    conditioner: int = 0
    washing_machine: int = 0
    safe: int = 0
    mini_bar: int = 0


class CreditCardCreate(BaseModel):
    number: str
    date: date


class UserCreate(BaseModel):
    first_name: str
    last_name: str
    surname: str | None = None
    birthdate: date
    email: str
    gender: Gender
    country: str
    credit_card: int | None = None


class SupportMessageCreate(BaseModel):
    user: int
    message: str


class UserCartCreate(BaseModel):
    user: int
    tour: int


class BoughtToursCreate(BaseModel):
    user: int
    tour: int
