from fastapi import APIRouter

from app.api.v1.endpoints import bought_tours, cart, hotel, liveness_router, support, tour, user

api_router = APIRouter()
api_router.include_router(liveness_router, tags=["liveness"])
api_router.include_router(bought_tours.router, prefix="/bought_tours", tags=["Bought tours"])
api_router.include_router(cart.router, prefix="/cart", tags=["Cart"])
api_router.include_router(user.router, prefix="/user", tags=["User"])
api_router.include_router(tour.router, prefix="/tours", tags=["Tours"])
api_router.include_router(support.router, prefix="/support", tags=["Support"])
api_router.include_router(hotel.router, prefix="/hotel", tags=["Hotel"])
