from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.database.base import get_session
from app.database.tables import User
from app.shemas.models import UserCreate

router = APIRouter()


@router.post("/user", response_model=UserCreate)
def create_user(
    user_data: UserCreate,
    session: Session = Depends(get_session),
):
    session.add(User(**user_data))
    session.commit()
    return user_data


@router.get("/{user_id}", response_model=UserCreate)
def get_user(
    user_id: int,
    session: Session = Depends(get_session),
):
    return session.query(User).filter(User.id == user_id).first()


@router.patch("/{user_id}", response_model=UserCreate)
def patch_user(
    user_id: int,
    new_data: UserCreate,
    session: Session = Depends(get_session),
):
    user = session.query(User).filter(User.id == user_id).first()
    user.birthdate = new_data.birthdate
    user.country = new_data.country
    user.credit_card = new_data.credit_card
    user.email = new_data.email
    user.first_name = new_data.first_name
    user.last_name = new_data.last_name
    user.surname = new_data.surname
    user.gender = user.gender
    session.commit()
