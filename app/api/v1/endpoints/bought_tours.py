from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.database.base import get_session
from app.database.tables import BoughtTours
from app.shemas.models import BoughtToursCreate

router = APIRouter()


@router.post("/bought_tour", response_model=BoughtToursCreate)
def create_buy(
    bought_tour_data: BoughtToursCreate,
    session: Session = Depends(get_session),
):
    session.add(BoughtTours(**bought_tour_data))
    session.commit()
    return bought_tour_data


@router.get("/bought_tour/{user_id}", response_model=BoughtToursCreate)
def get_bought_tours(
    user_id: int,
    session: Session = Depends(get_session),
):
    return session.query(BoughtTours).filter(BoughtTours.user == user_id).first()
