from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.database.base import get_session
from app.database.tables import Tour
from app.shemas.models import TourCreate

router = APIRouter()


@router.post("/tour", response_model=TourCreate)
def create_tour(
    tour_data: TourCreate,
    session: Session = Depends(get_session),
):
    session.add(Tour(**tour_data))
    session.commit()
    return tour_data


@router.get("/{tour_id}", response_model=TourCreate)
def get_tour(
    tour_id: int,
    session: Session = Depends(get_session),
):
    return session.query(Tour).filter(Tour.id == tour_id).first()
