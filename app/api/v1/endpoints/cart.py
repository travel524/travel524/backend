from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.database.base import get_session
from app.database.tables import UserCart
from app.shemas.models import UserCartCreate

router = APIRouter()


@router.post("/user_cart", response_model=UserCartCreate)
def create_user_cart(
    user_cart_data: UserCartCreate,
    session: Session = Depends(get_session),
):
    session.add(UserCart(**user_cart_data))
    session.commit()
    return user_cart_data


@router.get("/user_cart/{user_id}", response_model=UserCartCreate)
def get_user_cart(
    user_id: int,
    session: Session = Depends(get_session),
):
    return session.query(UserCart).filter(UserCart.user == user_id).first()
