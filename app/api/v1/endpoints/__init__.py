from fastapi import APIRouter

liveness_router = APIRouter()


@liveness_router.get("/liveness")
def liveness() -> str:
    return "success"
