from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.database.base import get_session
from app.database.tables import Hotel
from app.shemas.models import HotelCreate

router = APIRouter()


@router.post("/hotel", response_model=HotelCreate)
def create_hotel(
    hotel_data: HotelCreate,
    session: Session = Depends(get_session),
):
    session.add(Hotel(**hotel_data))
    session.commit()
    return hotel_data


@router.get("/{hotel_id}", response_model=HotelCreate)
def get_hotel(
    tour_id: int,
    session: Session = Depends(get_session),
):
    return session.query(Hotel).filter(Hotel.id == tour_id).first()
