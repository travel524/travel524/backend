from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.database.base import get_session
from app.database.tables import SupportMessage
from app.shemas.models import SupportMessageCreate

router = APIRouter()


@router.post("/support_message", response_model=SupportMessageCreate)
def create_support_message(
    support_message_data: SupportMessageCreate,
    session: Session = Depends(get_session),
):
    session.add(SupportMessage(**support_message_data))
    session.commit()
    return support_message_data


@router.get("/{support_id}", response_model=SupportMessageCreate)
def get_support_message(
    support_message_id: int,
    session: Session = Depends(get_session),
):
    return session.query(SupportMessage).filter(SupportMessage.id == support_message_id).first()
