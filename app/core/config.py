from functools import cache
from typing import TYPE_CHECKING

from app.core.settings.base import AppEnvTypes, BaseAppSettings
from app.core.settings.development import DevAppSettings
from app.core.settings.production import ProdAppSettings
from app.core.settings.test import TestAppSettings

if TYPE_CHECKING:  # pragma: no cover
    from app.core.settings.app import AppSettings

environments: dict[AppEnvTypes, type["AppSettings"]] = {
    AppEnvTypes.DEV: DevAppSettings,
    AppEnvTypes.PROD: ProdAppSettings,
    AppEnvTypes.TEST: TestAppSettings,
}


@cache
def get_app_settings() -> "AppSettings":
    app_env = BaseAppSettings().app_env
    config = environments[app_env]
    return config()


settings = get_app_settings()
