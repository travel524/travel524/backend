from enum import Enum

from pydantic_settings import BaseSettings, SettingsConfigDict


class AppEnvTypes(str, Enum):
    PROD = "prod"
    DEV = "dev"
    TEST = "test"


class BaseAppSettings(BaseSettings):
    app_env: AppEnvTypes = AppEnvTypes.PROD
    model_config = SettingsConfigDict(env_file=".env", extra="ignore")
