from typing import ClassVar

from pydantic import PostgresDsn, field_validator
from pydantic_core.core_schema import ValidationInfo
from pydantic_settings import SettingsConfigDict
from yarl import URL

from app.core.settings.base import BaseAppSettings


class AppSettings(BaseAppSettings):
    postgres_db_prefix: ClassVar[str] = ""

    project_name: str

    internal_host: str = "localhost"
    internal_port: int = 8000

    postgres_server: str
    postgres_user: str
    postgres_password: str
    postgres_db: str
    database_uri: PostgresDsn | None = None

    @field_validator("database_uri", mode="before")
    def assemble_db_connection(cls, v: str | None, info: ValidationInfo) -> str:  # noqa: N805
        return str(
            URL.build(
                scheme="postgresql+psycopg",
                user=info.data.get("postgres_user"),
                password=info.data.get("postgres_password"),
                host=info.data.get("postgres_server", ""),
                path=f"/{cls.postgres_db_prefix}{info.data.get('postgres_db') or ''}",
            ),
        )

    model_config = SettingsConfigDict(
        env_file=".env",
        env_file_encoding="utf-8",
        extra="ignore",
        env_nested_delimiter="__",
    )
