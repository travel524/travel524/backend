from typing import ClassVar

from pydantic import HttpUrl
from pydantic_core import Url

from app.core.settings.app import AppSettings


class TestAppSettings(AppSettings):
    title: str = "Test Travel-524"
    postgres_db_prefix: ClassVar[str] = "test_"
    nvd_api_base_url: HttpUrl = Url("https://example.com")
